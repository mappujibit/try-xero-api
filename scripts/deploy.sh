
#!/bin/sh -e

SERVICE_NAME=$(cat package.json \
  | grep '"name"' \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

docker-compose run --rm api /bin/sh -c "yarn install; yarn run build"

docker build --no-cache -t $SERVICE_NAME .

docker save $SERVICE_NAME | ssh -C deploy@staging docker load
ssh -C deploy@staging docker-compose up
