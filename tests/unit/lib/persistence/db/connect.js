/* eslint-disable */
jest.mock('knex');
import 'babel-polyfill';
import * as connect from '../../../../../src/lib/persistence/db/connect';

describe('test db connection functions', () => {

  test('connectAsyncAndValidate function should works as expected', async () => {
    const conn = await connect.connectAsyncAndValidate('mysql://user:pass@localhost:3306');
    expect(conn).toBeInstanceOf(Object);
  })

  test('connect function should works as expected', () => {
    const conn = connect.connect('mysql://user:pass@localhost:3306');
    expect(conn).toBeInstanceOf(Object);
  })

})
