/* eslint-disable */
import 'babel-polyfill';
import container from '../../../../src/modules/core/container';
import { isFunction, isObject } from '../../../../src/lib/utils';

describe('core container module test', () => {

  test('container should works as expected', () => {
    expect(typeof container.has === 'function').toBeTruthy();
    expect(typeof container.get === 'function').toBeTruthy();
  })

  test('container should return requested service', async () => {
    // sentry should be turned off in testing
    expect(container.get('sentry')).toBeNull();

    try {
      expect(await container.get('sentryAsync')).toBeNull();
    } catch (err) {}
  })

})
