/* eslint-disable */
import api from '../../../../src/modules/core/api';
import { isFunction } from '../../../../src/lib/utils';

describe('core api module test', () => {

  test('api must be instance of express router', () => {
    expect(isFunction(api)).toBeTruthy();
  })

})
