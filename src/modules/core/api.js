import express from 'express';

const api = express.Router();

api.get('/', (req, res) => {
  res.status(200).json({ status: 'ok' });
});

export default api;
