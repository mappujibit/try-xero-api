import status from 'http-status';

export class RuntimeError extends Error {}
export class ServiceNotFoundError extends RuntimeError {}
export class ConfigurationError extends RuntimeError {}

/**
 * APIError
 */
export class APIError extends Error {
  constructor(msg, httpStatus, previousError) {
    if (msg instanceof Error) {
      previousError = msg;
      msg = previousError.message;
    }

    super(msg);
    this.httpStatus = httpStatus || status.INTERNAL_SERVER_ERROR;
    this.previousError = previousError;
  }
}

/**
 * AuthorizationError
 */
export class AuthorizationError extends APIError {
  constructor(msg, previousError) {
    super(msg, status.UNAUTHORIZED, previousError);
  }
}

/**
 * AuthenticationError
 */
export class AuthenticationError extends APIError {
  constructor(msg, previousError) {
    super(msg, status.FORBIDDEN, previousError);
  }
}

/**
 * BadRequestError
 */
export class BadRequestError extends APIError {
  constructor(msg, previousError) {
    super(msg, status.BAD_REQUEST, previousError);
  }
}

/**
 * InternalServerError
 */
export class InternalServerError extends APIError {
  constructor(msg, previousError) {
    super(msg, status.INTERNAL_SERVER_ERROR, previousError);
  }
}

/**
 * NotFoundError
 */
export class NotFoundError extends APIError {
  constructor(msg, previousError) {
    super(msg, status.NOT_FOUND, previousError);
  }
}
