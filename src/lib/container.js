import 'babel-polyfill';
import _ from 'lodash';
import Promise from 'bluebird';
import { isObject, isFunction } from './utils';
import { ServiceNotFoundError } from './errors';

export default class Container {
  /**
   * @param {Object} config
   * @return {Container}
   */
  constructor(config = {}) {
    if (!isObject(config)) {
      throw new Error('Config must be an object containing factory functions');
    }

    this.cache = {};
    this.config = {
      prototype: {},
      factory: {},
    };

    this.import(config);
  }

  /**
   * @param {String} name
   * @returns {Boolean}
   */
  has(name) {
    return _.includes(Object.keys(this.config.factory), name)
      || _.includes(Object.keys(this.config.prototype), name);
  }

  /**
   * @param {String} name
   * @returns {Promise}
   */
  async promisify(name) {
    try {
      const service = this.get(name);

      if (service instanceof Promise) {
        return service;
      }

      return Promise.resolve(service);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  /**
   * @param {Object} config
   */
  import(config) {
    const keys = Object.keys(this.config);
    keys.forEach((k) => {
      if (config[k]) {
        Object.keys(config[k]).forEach(name => this[k](name, config[k][name], false));
      }
    });
  }

  /**
   * @param {String} name
   * @param {Function} cb
   * @param {Boolean} force
   */
  factory(name, cb, force = false) {
    // eslint-disable-next-line no-underscore-dangle
    this._register(name, cb, force, false);
  }

  /**
   * @param {string} name
   * @param {function} cb
   * @param {boolean} force
   */
  prototype(name, cb, force = false) {
    // eslint-disable-next-line no-underscore-dangle
    this._register(name, cb, force, true);
  }

  /**
   * @param {string} name
   * @param {function(err, Object)} cb
   * @return {Object}
   */
  get(name, cb) {
    if (this.cache[name]) {
      return cb ? cb(null, this.cache[name]) : this.cache[name];
    }

    if (_.includes(Object.keys(this.config.factory), name)) {
      this.cache[name] = this.config.factory[name](this);
      return isFunction(cb) ? cb(null, this.cache[name]) : this.cache[name];
    }

    if (_.includes(Object.keys(this.config.prototype), name)) {
      const service = this.config.prototype[name](this);
      return isFunction(cb) ? cb(null, service) : service;
    }

    const err = new ServiceNotFoundError(`Service '${name}' does not exists.`);

    if (cb) {
      return cb(err);
    }

    throw err;
  }

  /**
   * @param {String} name
   * @param {Function} cb
   * @param {Boolean} force
   * @param {Boolean} isPrototype
   */
  _register(name, cb, force, isPrototype) {
    if (this.has(name) && !force) {
      throw new Error(`${name} is already registered, use force register instead.`);
    }

    if (!isFunction(cb)) {
      throw new Error(`Factory for ${name} must be a function.`);
    }

    if (isPrototype) {
      this.config.prototype[name] = cb;
    } else {
      this.config.factory[name] = cb;
    }
  }
}
